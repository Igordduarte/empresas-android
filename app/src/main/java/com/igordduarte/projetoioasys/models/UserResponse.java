package com.igordduarte.projetoioasys.models;

/**
 * Created by Igordduarte on 25/03/2018.
 */

public class UserResponse {
    private String investor_name;
    private String email;
    private String password;

    public String getInvestor_name() {
        return investor_name;
    }

    public void setInvestor_name(String investor_name) {
        this.investor_name = investor_name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
