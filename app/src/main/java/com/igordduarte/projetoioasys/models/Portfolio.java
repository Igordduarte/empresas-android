package com.igordduarte.projetoioasys.models;

public class Portfolio {

    private int enterprises_number;
    private int enterprises;

    public int getEnterprises_number() {
        return enterprises_number;
    }

    public void setEnterprises_number(int enterprises_number) {
        this.enterprises_number = enterprises_number;
    }

    public int getEnterprises() {
        return enterprises;
    }

    public void setEnterprises(int enterprises) {
        this.enterprises = enterprises;
    }
}
