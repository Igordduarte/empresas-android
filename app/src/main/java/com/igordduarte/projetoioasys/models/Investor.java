package com.igordduarte.projetoioasys.models;

import java.io.Serializable;

/**
 * Created by Igordduarte on 25/03/2018.
 */

public class Investor implements Serializable {
    private String id;
    private String investor_name;
    private String email;
    private String city;
    private String country;
    private String balance;
    private String photo;
    private Portfolio portfolio;
    private int portfolio_value;
    private boolean first_access;
    private boolean super_angel;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getInvestor_name() {
        return investor_name;
    }

    public void setInvestor_name(String investor_name) {
        this.investor_name = investor_name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getBalance() {
        return balance;
    }

    public void setBalance(String balance) {
        this.balance = balance;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public Portfolio getPortfolio() {
        return portfolio;
    }

    public void setPortfolio(Portfolio portfolio) {
        this.portfolio = portfolio;
    }

    public int getPortfolio_value() {
        return portfolio_value;
    }

    public void setPortfolio_value(int portfolio_value) {
        this.portfolio_value = portfolio_value;
    }

    public boolean isFirst_access() {
        return first_access;
    }

    public void setFirst_access(boolean first_access) {
        this.first_access = first_access;
    }

    public boolean isSuper_angel() {
        return super_angel;
    }

    public void setSuper_angel(boolean super_angel) {
        this.super_angel = super_angel;
    }
}
