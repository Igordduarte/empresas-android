package com.igordduarte.projetoioasys.models;

import java.io.Serializable;

/**
 * Created by Igordduarte on 25/03/2018.
 */

public class AccessToken implements Serializable {

    private String access_token;
    private String client;
    private String uid;

    public AccessToken() {

    }

    public String getAccess_token() {
        return access_token;
    }

    public void setAccess_token(String access_token) {
        this.access_token = access_token;
    }

    public String getClient() {
        return client;
    }

    public void setClient(String client) {
        this.client = client;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }
}