package com.igordduarte.projetoioasys.app;

import com.igordduarte.projetoioasys.models.EnterpriseList;
import com.igordduarte.projetoioasys.models.UserResponse;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;

/**
 * Created by Igordduarte on 25/03/2018.
 */

public interface Constants {
    String BASE_URL =" http://54.94.179.135:8090/api/v1/";
    String SHARED_KEY="Arquivo_token";

    @POST("users/auth/sign_in")
    Call<UserResponse>getUserAccess(@Body UserResponse userRequest);
    @GET("enterprises")
    Call<EnterpriseList>getEnterprises(@Header("access-token") String access_token, @Header("client") String client,
                                       @Header("uid") String uid);

}
