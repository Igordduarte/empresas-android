package com.igordduarte.projetoioasys.fragments;

import android.app.Fragment;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.igordduarte.projetoioasys.R;
import com.igordduarte.projetoioasys.adapters.ListAdapter;
import com.igordduarte.projetoioasys.app.Constants;
import com.igordduarte.projetoioasys.models.AccessToken;
import com.igordduarte.projetoioasys.models.EnterpriseList;
import com.igordduarte.projetoioasys.models.Investor;
import com.igordduarte.projetoioasys.ui.CompanyDetails;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Igordduarte on 25/03/2018.
 */

public class SearchFragment extends Fragment implements ListAdapter.OnItemClickInterface {

    private EnterpriseList enterpriseList;
    private ListAdapter adapter;
    private EditText searchField;
    private RelativeLayout fragment;

    public SearchFragment() {
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.list_fragment, container, false);

        searchField = (EditText) view.findViewById(R.id.search_search_field);
        fragment = (RelativeLayout) view.findViewById(R.id.fragment);

        final RecyclerView lista_empresas = (RecyclerView) view.findViewById(R.id.recycler_view);

        lista_empresas.setLayoutManager(new LinearLayoutManager(getContext()));
        adapter = new ListAdapter(getContext(), this);
        lista_empresas.setAdapter(adapter);

        Retrofit retrofit = new Retrofit.Builder().baseUrl(Constants.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create()).build();
        AccessToken accessToken = new AccessToken();
        SharedPreferences shared = this.getActivity().getSharedPreferences(Constants.SHARED_KEY, 0);
        accessToken.setAccess_token(shared.getString("access_token", "sem Token"));
        accessToken.setClient(shared.getString("client", "Sem Client"));
        accessToken.setUid(shared.getString("uid", "Sem Uid"));
        //call
        final Constants serviseEnterprises = retrofit.create(Constants.class);
        Call<EnterpriseList> enterprisesCall = serviseEnterprises.getEnterprises(accessToken.getAccess_token(),
                accessToken.getClient(), accessToken.getUid());

        enterprisesCall.enqueue(new Callback<EnterpriseList>() {
            @Override
            public void onResponse(Call<EnterpriseList> call, Response<EnterpriseList> response) {

                if (response.isSuccessful()) {
                    enterpriseList = response.body();

                } else {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        Toast.makeText(getContext(), "Token negado", Toast.LENGTH_SHORT).show();
                    }
                }
            }

            @Override
            public void onFailure(Call<EnterpriseList> call, Throwable t) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    Toast.makeText(getContext(), "Sem resposta", Toast.LENGTH_SHORT).show();
                }
            }
        });

        searchField.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                String nome_enterprise = searchField.getText().toString();
                List<Investor> lista = getEnterprises(nome_enterprise);
                adapter.setListInvestors(lista);
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });

        searchField.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                final int DRAWABLE_LEFT = 0;
                final int DRAWABLE_TOP = 1;
                final int DRAWABLE_RIGHT = 2;
                final int DRAWABLE_BOTTOM = 3;

                if (event.getAction() == MotionEvent.ACTION_UP) {
                    if (event.getRawX() >= (searchField.getRight() - searchField.getCompoundDrawables()[DRAWABLE_RIGHT].getBounds().width())) {
                        fragment.setVisibility(View.GONE);
                        return true;
                    }
                }
                return false;
            }
        });
        return view;
    }

    private List<Investor> getEnterprises(String nome_enterprise) {
        List<Investor> lista = new ArrayList<Investor>();
        for (int i = 0; i < enterpriseList.enterprises.size(); i++) {
            Investor investor = (Investor) enterpriseList.enterprises.get(i);

            if (investor.getInvestor_name().startsWith(nome_enterprise)) {
                lista.add(investor);
            }
        }
        return lista;
    }

    public void onItemClick(Investor investor) {
        Intent intent = null;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
            intent = new Intent(getContext(), CompanyDetails.class);
        }
        intent.putExtra(CompanyDetails.EXTRA_KEY, investor);
        startActivity(intent);
    }
}
