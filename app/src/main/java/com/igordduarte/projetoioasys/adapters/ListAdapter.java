package com.igordduarte.projetoioasys.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.igordduarte.projetoioasys.R;
import com.igordduarte.projetoioasys.models.Investor;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Igordduarte on 25/03/2018.
 */

public class ListAdapter extends RecyclerView.Adapter<ListAdapter.ViewHolder> {
    private List<Investor> listInvestors;
    private Context context;
    private OnItemClickInterface onItemClickInterface;

    public ListAdapter(Context context, OnItemClickInterface onItemClickInterface) {
        this.context = context;
        this.onItemClickInterface = onItemClickInterface;
        listInvestors = new ArrayList<Investor>();
    }

    public void setListInvestors(List<Investor> listInvestors) {
        this.listInvestors = listInvestors;
        notifyDataSetChanged();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.list_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        Investor investor = listInvestors.get(position);
        holder.item_name.setText(investor.getInvestor_name());
        holder.item_country.setText(investor.getCountry());

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onItemClickInterface.onItemClick(listInvestors.get(holder.getAdapterPosition()));
            }
        });
    }


    @Override
    public int getItemCount() {
        return listInvestors.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private View view;
        private TextView item_name;
        private TextView item_type;
        private TextView item_country;
        private int adapterPosition;

        public ViewHolder(View itemView) {
            super(itemView);
            this.view = itemView;
            this.item_name = (TextView) view.findViewById(R.id.item_name_label);
            this.item_type = (TextView) view.findViewById(R.id.item_type_label);
            this.item_country = (TextView) view.findViewById(R.id.item_country_label);
        }

    }

    public interface OnItemClickInterface {
        void onItemClick(Investor item);
    }
}