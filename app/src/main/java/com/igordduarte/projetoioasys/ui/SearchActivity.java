package com.igordduarte.projetoioasys.ui;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.widget.EditText;

import com.igordduarte.projetoioasys.R;

public class SearchActivity extends AppCompatActivity {

    private EditText searchField;
    private RecyclerView listRecyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);
        getSupportActionBar().hide();


        searchField = findViewById(R.id.search_search_field);
        listRecyclerView = findViewById(R.id.recycler_view);


    }
}
