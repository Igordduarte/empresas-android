package com.igordduarte.projetoioasys.ui;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.widget.TextView;

import com.igordduarte.projetoioasys.R;
import com.igordduarte.projetoioasys.models.Investor;

public class CompanyDetails extends AppCompatActivity {

    public static final String EXTRA_KEY = "empresa";
    private Investor investor;
    private TextView detailLabel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_company_details);
        Intent intent = getIntent();

        investor = (Investor) intent.getSerializableExtra(EXTRA_KEY);
        detailLabel = (TextView)findViewById(R.id.company_description_label);

    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }
}