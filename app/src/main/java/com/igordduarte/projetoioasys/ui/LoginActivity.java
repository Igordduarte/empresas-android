package com.igordduarte.projetoioasys.ui;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.igordduarte.projetoioasys.R;
import com.igordduarte.projetoioasys.app.Constants;
import com.igordduarte.projetoioasys.models.UserResponse;

import okhttp3.Headers;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class LoginActivity extends AppCompatActivity {

    private EditText emailLabel;
    private EditText passwordLabel;
    private Button enterButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        getSupportActionBar().hide();

        emailLabel = (EditText) findViewById(R.id.login_email_field);
        passwordLabel = (EditText) findViewById(R.id.login_password_field);
        enterButton = (Button) findViewById(R.id.login_enter_button);

        Retrofit retrofit = new Retrofit.Builder().baseUrl(Constants.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create()).build();
        final Constants servise = retrofit.create(Constants.class);

        enterButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                UserResponse userRequest = new UserResponse();
                userRequest.setEmail(emailLabel.getText().toString());
                userRequest.setPassword(passwordLabel.getText().toString());
                Call<UserResponse> tokenResponseCall = servise.getUserAccess(userRequest);

                tokenResponseCall.enqueue(new Callback<UserResponse>() {
                    @Override
                    public void onResponse(Call<UserResponse> call, Response<UserResponse> response) {

                        if (response.isSuccessful()) {
                            SharedPreferences sharedPreferences = getSharedPreferences(Constants.SHARED_KEY, 0);
                            SharedPreferences.Editor editor = sharedPreferences.edit();
                            UserResponse userResponse = response.body();
                            Headers headers = response.headers();
                            String accessToken = headers.get("access-token");
                            String client = headers.get("client");
                            String uid = headers.get("uid");
                            editor.putString("access_token", accessToken);
                            editor.putString("client", client);
                            editor.putString("uid", uid);
                            editor.commit();
                            Intent intent = new Intent(LoginActivity.this, HomeActivity.class);
                            intent.putExtra("AccessToken", accessToken);
                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(intent);
                        } else {
                            Toast.makeText(getApplicationContext(), "Usuário invalido", Toast.LENGTH_SHORT).show();
                        }

                    }

                    @Override
                    public void onFailure(Call<UserResponse> call, Throwable t) {
                        Toast.makeText(getApplicationContext(), t.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                });
            }
        });
    }
}